
function AttendeesList(props) {
    return (
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Conference</th>
              </tr>
            </thead>
            <tbody>
              {props.attendees.map(attendee => {
                return (
                  // without a key added to tr tag in line 27, we get a warning
                  // "each child in a list should have a unique key prop."
                  // add a key to the tr tag and give it a value unique to the attendee
                  <tr key={ attendee.href }>
                    <td>{ attendee.name }</td>
                    <td>{ attendee.conference }</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
      );
}

export default AttendeesList;
