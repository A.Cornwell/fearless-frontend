// create a createCard function, copying the card HTML from the index.html file
    // and putting it into a backtick string
function createCard(name, location, description, pictureUrl, starts, ends) {
    return `
      <div class="card mb-3 shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${new Date(starts).toLocaleDateString()} -
        ${new Date(ends).toLocaleDateString()}
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        // throw new Error("test");
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
        // const alert = "Error occured";
        // const alertTag = document.querySelector('#danger');
        // alertTag.innerHTML = alert;
      } else {
        // wait for the json response to turn into a js object
        const data = await response.json();

        // access first conference in the data object
        // const conference = data.conferences[0];

        let index = 0;
        for (let conference of data.conferences){
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const location = details.conference.location.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const starts = details.conference.starts;
                const ends = details.conference.ends;
                const html = createCard(title, location, description, pictureUrl, starts, ends);
                const column = document.querySelector(`#col-${index % 3}`);
                column.innerHTML += html;
                index +=1;
            }
        }

        // input the conference name to the index.html file

        // const nameTag = document.querySelector('.card-title');
        // nameTag.innerHTML = conference.name;

        // get the conference details

        // const detailUrl = `http://localhost:8000${conference.href}`;
        // const detailResponse = await fetch(detailUrl);
        // if (detailResponse.ok) {
        //     // assign all conference details to variable "details"

        // //   const details = await detailResponse.json();

        //     //  get the descripton

        //     const confDescription = details.conference.description;

        //     // input the conference description to the index.html file
        //     const descriptionTag = document.querySelector('.card-text');
        //     descriptionTag.innerHTML = confDescription;

        //     // get the image url

        //     const confImage = details.conference.location.picture_url;

        //     // input image within index.html file
        //     const imageTag = document.querySelector('.card-img-top');
        //     imageTag.innerHTML = confImage;
        //     imageTag.src = details.conference.location.picture_url;
        // }




      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error(e)
      const alert = document.querySelector('.error');
      alert.innerHTML = `
      <div class="alert alert-danger" role="alert">
      This is an error message
      </div>
      `;
    }

  });
