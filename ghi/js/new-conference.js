window.addEventListener('DOMContentLoaded', async () => {

    // get the locations
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (!response.ok) {
        console.log("Invalid response, try again!");

    } else {
        // if the response was ok get the json data from our request.
        const data = await response.json();
        const selectTag = document.getElementById('location');
        // populate the dropdown menu
        // For each location in the location property of the database
        for (const location of data.locations) {
            // create an option element
            const option =  document.createElement("option");
            option.innerHTML = location.name;
             // Set the '.value' property of the new option element to the
            // location id
            option.value = location.id;
            //  // Append the option element as a child of the select tag
            selectTag.appendChild(option);
        }
    }


    // upon "submit", get the form data the user entered
    const formTag = document.getElementById('create-conference-form');
    // add event listener for the "submit" event
    formTag.addEventListener('submit', async event => {
        // prevent default behavior of browser
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));


        // send the data to the server
        const conferencesUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
        },
    };
        const response = await fetch(conferencesUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
        } else {
            console.log("Invalid JSON content.");

    }

  });
});
