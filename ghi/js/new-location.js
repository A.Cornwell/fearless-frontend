window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    const response = await fetch(url);


    if (response.ok) {
        const data = await response.json();

        // Get the select tag element by its id "state"
        const selectTag = document.getElementById('state');

        // populate the dropdown menu
        // For each state in the state property of the database
        for (let state of data.states) {
            // create an option element
            const option =  document.createElement("option");
             // Set the '.value' property of the new option element to the
            // state's abbreviation
            option.value = state.abbreviation;
            // // Set the '.innerHTML' property of the new option element to
            // the state's name
            option.innerHTML = state.name;
            //  // Append the option element as a child of the select tag
            selectTag.appendChild(option);



        }
    }
    // select the form element form html file
    const formTag = document.getElementById('create-location-form');
    // add event listener for the "submit" event
    formTag.addEventListener('submit', async event => {
        // prevent default behavior of browser
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
        },
    };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
    }
  });
});
